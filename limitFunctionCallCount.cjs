function limitFunctionCallCount(cb, n) {
  function limiter() {
    if (n != 0) {
      n--;
      return cb();
    } else {
      return null;
    }
  }

  return limiter;
}
module.exports = limitFunctionCallCount;
