const cacheFunction = require("../cacheFunction.cjs");

let callBack = (cache, value) => {
  console.log("Callback is invoked");
  return (cache[value] = value);
};

const result = cacheFunction(callBack);

//When we call the function with argument for the first time.
//It will invoke the callback function
result("Sam");
result(456);
result("Ram");
result("Gowtham");

//When we invoke the function with arguments we already used
//It will return the result from the cache directly.
//Do not invoke the callback function again
result("Sam");
result(456);
