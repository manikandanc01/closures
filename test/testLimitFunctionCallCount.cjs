const limitFunctionCall = require("../limitFunctionCallCount.cjs");

let callBack = function () {
  console.log("Callback is invoked");
};

let n = 2;
const result = limitFunctionCall(callBack, n);

//We can only invoke the callback function for n times
for (let index = 0; index < n; index++) {
  result();
}

//After n times we can't invoke the callBack function
console.log(result()); //return null
