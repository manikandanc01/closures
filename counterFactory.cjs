function counterFactory() {
    let counter = 0;
    
    let result = {
        increment: function () {
            return ++counter;
        },
        decrement: function () {
            return --counter;
        },
    };

    return result;
}
module.exports = counterFactory;