function cacheFunction(cb) {
  const cache = {};
  function cacheHelper(value) {
    //If passed value is not present in the cache invoke call back function
    //Else returns the already stored value
    if (cache[value] === undefined) {
      return cb(cache, value);
    } else {
      console.log("Callback is not invoked");
      return cb[cache];
    }
  }

  return cacheHelper;
}
module.exports = cacheFunction;
